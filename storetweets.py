import tweepy #https://github.com/tweepy/tweepy
import json
import credentials
from firebase import post_to_firestore
import time

#Twitter API credentials
consumer_key = credentials.consumer_key
consumer_secret = credentials.consumer_secret
access_key = credentials.access_token
access_secret = credentials.access_token_secret

def get_last_100_tweets(screen_name):
	#Twitter only allows access to a users most recent 3240 tweets with this method
	#authorize twitter, initialize tweepy
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	
	#initialize a list to hold all the tweepy Tweets
	alltweets = []	
	
	#make initial request for most recent tweets (200 is the maximum allowed count)
	new_tweets = api.user_timeline(screen_name=screen_name, count=20)
	
	#save most recent tweets
	alltweets.extend(new_tweets)
	
	#save the id of the oldest tweet less one
	oldest = alltweets[-1].id - 1
	#keep grabbing tweets until there are no tweets left to grab
	while len(alltweets) < 100:
		print ("getting tweets before %s" % (oldest))
		
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(screen_name = screen_name,count=20,max_id=oldest)
		
		#save most recent tweets
		alltweets.extend(new_tweets)
		
		#update the id of the oldest tweet less one
		oldest = alltweets[-1].id - 1
		
		print ("...%s tweets downloaded so far" % (len(alltweets)))

	
	for i in range(0,len(alltweets)):
		post_to_firestore((alltweets[i]))
	# with open('tweets.json', "wa") as f:
	# 	# f.write(json.dump(new_tweets))
	# 	json.dump(new_tweets.decode('utf-8'), f)
	
	#transform the tweepy tweets into a 2D array that will populate the csv	
	# outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode("utf-8")] for tweet in alltweets]
	
	#write the csv	
	# with open('%s_tweets.csv' % screen_name, 'wb') as f:
	# 	writer = csv.writer(f)
	# 	writer.writerow(["id","created_at","text"])
	# 	writer.writerows(outtweets)
	
	# pass


if __name__ == '__main__':
	while 1 > 0:
		#pass in the username of the account you want to download
		get_last_100_tweets("ma3route")
		get_last_100_tweets("KenyanTraffic")
		print("Sleeping...")
		time.sleep(20)