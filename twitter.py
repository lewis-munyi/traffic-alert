from __future__ import absolute_import, print_function

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json
import credentials
# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key = credentials.consumer_key
consumer_secret = credentials.consumer_secret
# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token = credentials.access_token
access_token_secret = credentials.access_token_secret
class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def on_data(self, data):
        tweet = json.loads(data)

        # print('Tweet by: ' + tweet.user.name)
        # print('Tweet about: ' + data.text)
        # print('Tweet created on: ' + str(data.created_at))
        print(tweet)
        return True

    def on_error(self, status):
        print(status)

keywords = ['ngong rd','ngong road','ngong\' rd','waiyaki way', 'kiambu rd', 'kiambu road', 'msa rd', 'mombasa road', 'mombasa rd', "thika rd", "thika road", "thika"]


if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)



    stream = Stream(auth, l)
    for item in keywords:
        stream.filter(track=[item])